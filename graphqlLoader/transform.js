/* eslint-disable no-underscore-dangle */
/* eslint-disable camelcase */
/* eslint-disable no-param-reassign */

module.exports = episodes => [episodes[0], episodes[1]].reduce((_result, _episode) => {
//   console.log(_episode);
  const episode = {
    title: _episode.title,
    url: _episode.link,
    mp3: _episode.mp3File,
    img: _episode.img,
    published: new Date(_episode.releaseDate),
    season: _episode.episode.season,
    episode: _episode.episode.episode,
    id: `S${_episode.episode.season}E${_episode.episode.episode}`,
  };

  const {
    artists,
    artists_tracks,
    labels,
    tracks,
    plays,
  } = _episode.tracks.reduce((r, track, i) => {
    const _label = { name: track.label };
    const _track = { title: track.track, id: track.track };
    const _artists = [{ name: track.artist, id: track.artist }];
    const _artists_tracks = _artists
      .map(({ id }) => ({ artist_id: id, track_id: _track.id, type: '' }));
    const _play = {
      episode_id: episode.id, track_id: _track.id, order: i + 1, raw: track.rawTrack,
    };
    return {
      artists: [...r.artists, ..._artists],
      artists_tracks: [...r.artists_tracks, ..._artists_tracks],
      labels: [...r.labels, _label],
      tracks: [...r.tracks, _track],
      plays: [...r.plays, _play],
    };
  }, {
    artists: [], artists_tracks: [], tracks: [], labels: [], plays: [],
  });

  return {
    episodes: [..._result.episodes, episode],
    labels: [..._result.labels, ...labels],
    artists: [..._result.artists, ...artists],
    tracks: [..._result.tracks, ...tracks],
    artists_tracks: [..._result.artists_tracks, ...artists_tracks],
    plays: [..._result.plays, ...plays],
  };
}, {
  labels: [], artists: [], tracks: [], artists_tracks: [], episodes: [], plays: [],
});
