const fs = require('fs-extra');
const client = require('./loadGraphqlClient');
const { BULK_IMPORT } = require('./graphql/mutations');
const transform = require('./transform');

// eslint-disable-next-line no-unused-vars
const mutate = variables => client.mutate({
  mutation: BULK_IMPORT,
  variables,
});

fs.readJSON('../data/data.json')
  .then(transform)
  .then(mutate)
  .then(console.log)
  .catch(e => console.log(e.message));
