require('dotenv').config();
const { ApolloClient } = require('apollo-boost');
const { fetch } = require('cross-fetch/polyfill');
const { createHttpLink } = require('apollo-link-http');
const { InMemoryCache } = require('apollo-cache-inmemory');

const client = new ApolloClient({
  link: createHttpLink({
    uri: process.env.API,
    fetch,
    headers: {
      'x-hasura-admin-secret': process.env.PASS,
    },
  }),
  cache: new InMemoryCache(),
});

module.exports = client;
