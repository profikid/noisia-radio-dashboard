const gql = require('graphql-tag');

const BULK_IMPORT = gql`
  mutation upsertArtist(
    $artists: [artists_insert_input!]! 
    $labels: [labels_insert_input!]!
    $episodes: [episodes_insert_input!]!
    $artists_tracks: [artists_tracks_insert_input!]!
    $plays: [plays_insert_input!]!
    $tracks: [tracks_insert_input!]!
  ) {
    
    insert_artists(
      objects: $artists 
      on_conflict: {
        update_columns: updated_at constraint: artists_pkey
      }) {
      affected_rows
    }
    insert_labels(objects: $labels) {
      affected_rows
    }
    insert_tracks(objects: $tracks) {
      affected_rows
    }
    insert_artists_tracks(objects: $artists_tracks) {
      affected_rows
    }
    insert_episodes(objects: $episodes) {
      affected_rows
    }
    insert_plays(objects: $plays) {
      affected_rows
    }
  }
`;

module.exports = {
  BULK_IMPORT,
};
