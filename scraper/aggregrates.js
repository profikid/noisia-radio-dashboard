const fs = require('fs-extra');

const aggregration = 'artist';

fs.readJson('../data/tracks.json')
  .then((tracks) => {
    const groupCount = {};
    tracks.forEach((track) => {
      if (groupCount[track[aggregration]] === undefined) {
        groupCount[track[aggregration]] = 0;
      }
      // eslint-disable-next-line no-plusplus
      groupCount[track[aggregration]]++;
    });
    return groupCount;
  })
  .then(x => fs.writeJson(`${aggregration}_aggregration.json`, x, { spaces: '\t' }));
