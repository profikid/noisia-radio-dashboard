module.exports = s => [...s.split('&')[0].split(','), s.split('&')[1]].filter(Boolean).map(x => x.trim());
