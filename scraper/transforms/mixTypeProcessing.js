module.exports = (mt) => {
  const types = ['Remix', 'Feat.', 'Ft.', 'Edit'];
  const type = null;
  const artist = null;
  let result = { mt, artist, type };
  // eslint-disable-next-line no-shadow
  types.forEach((type) => {
    if (mt.includes(type)) {
      result = { mt, artist: mt.replace(type, '').trim(), type };
    }
  });
  return result;
};
