/* eslint-disable no-param-reassign */
const fs = require('fs-extra');
const splitArtist = require('./splitArtist');
const mixTypeProcessing = require('./mixTypeProcessing');
const fileMeta = require('./fileMeta');

fs.readJson('../data/data.json')
  .then((episodes) => {
    let tracks = [];
    episodes.forEach((epi) => {
      const episode = Object.assign({}, epi);
      delete episode.tracks;

      epi.tracks.forEach((track, i) => {
        track.order = i;
        track.episode = episode;
        if (typeof track.artist === 'string') {
          track.artist = splitArtist(track.artist);
          if (track.isCollab === undefined) {
            track.isCollab = track.artist.length > 1;
          }
          if (track.mixType.length) {
            const mt = mixTypeProcessing(track.mixType);
            if (mt.artist !== null) {
              track.artist.push(mt.artist);
              track.mixType = mt.type;
            }
          }
          if (track.episode.fileInfo.duration === undefined) {
            track.episode.fileInfo = fileMeta(track.episode.fileInfo.raw);
          }
        }
      });
      tracks = [...tracks, ...epi.tracks];
    });
    return tracks.sort((a, b) => new Date(b.episode.releaseDate) - new Date(a.episode.releaseDate));
  })
  .then(x => fs.writeJson('../data/tracks.json', x, { spaces: '\t' }));
