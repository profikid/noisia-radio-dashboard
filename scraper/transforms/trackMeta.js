const splitArtist = require('./splitArtist');

const strip = arr => arr[0].slice(1, -1);

module.exports = (rawTrack) => {
  const noop = '';
  const mixTypeRegex = /\((.*?)\)/g;
  const labelRegex = /\[(.*?)\]/g;
  let label = rawTrack.match(labelRegex) || noop;
  let mixType = rawTrack.match(mixTypeRegex) || noop;
  let artist = rawTrack.slice(0, rawTrack.indexOf('-')).trim() || noop;
  const track = rawTrack.slice(rawTrack.indexOf('-') + 1, rawTrack.indexOf('[')).trim() || noop;

  artist = splitArtist(artist);
  console.log(artist);

  artist.isCollab = artist.length > 1;

  if (label !== noop) {
    label = strip(label);
  }
  if (mixType !== noop) {
    mixType = strip(mixType);
  }

  return {
    artist,
    track,
    label,
    mixType,
    rawTrack,
  };
};
