const toSec = (str) => {
  const p = str.split(':');
  let s = 0; let
    m = 1;

  while (p.length > 0) {
    s += m * parseInt(p.pop(), 10);
    m *= 60;
  }

  return s;
};

module.exports = (raw) => {
  const s = raw.split(' ');
  const fileType = s[1];
  const size = s[4];
  const duration = s[7];
  const durationInSeconds = toSec(duration);
  return {
    raw,
    fileType,
    size,
    duration,
    durationInSeconds,
  };
};
