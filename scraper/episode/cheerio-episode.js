const cheerio = require('cheerio');
const fetch = require('node-fetch');
const trackMeta = require('../transforms/trackMeta');
const fileInfoMeta = require('../transforms/fileMeta');

module.exports = link => fetch(link)
  .then(x => x.text())
  .then(x => cheerio.load(x))
  .then(($) => {
    const infoDom = $('.episodebox');
    const title = $('.episode_title').text();
    const episodeArr = title.match(/^S?\d*\.\d+|[-]?\d+/g);
    const episode = {
      season: episodeArr[0],
      episode: episodeArr[1],
    };
    const img = infoDom.find('img').attr('src');
    const releaseDate = infoDom.find('.episode_date').text();
    const pDom = infoDom.find('div>p');
    const tracks = [];
    let fileInfoRaw = '';
    const mp3File = $('.btn.btn-default').attr('href');

    pDom.each(function pushTrack() {
      const t = $(this).text();
      if (t.includes(' - ') && t.includes('[') && t.includes(']')) {
        tracks.push(trackMeta(t.trim()));
      }
      if (t.includes('Filetype')) {
        fileInfoRaw = fileInfoMeta(t);
      }
    });

    return {
      title,
      link,
      episode,
      img,
      releaseDate,
      tracks,
      mp3File,
      fileInfo: fileInfoRaw,
    };
  });
