const fs = require('fs-extra');
const episodeScrape = require('./cheerio-episode');

const dataFolder = '../data';
const linksPath = `${dataFolder}/links.json`;
const dataPath = `${dataFolder}/data.json`;

(async () => {
  const links = await fs.readJson(linksPath);
  const data = await fs.readJson(dataPath);
  const toDoLinks = links.filter(l => l.done === undefined);
  const toDoLink = toDoLinks[0];
  if (toDoLink) {
    const episode = await episodeScrape(toDoLink.href);
    data.push(episode);
    await fs.writeJson(dataPath, data, { spaces: '\t' });
    links[links.indexOf(toDoLink)].done = true;
    await fs.writeJson(linksPath, links, { spaces: '\t' });
  } else {
    console.log('NOTHING TO SCRAPE');
  }
  console.log('TOTAL LINKS', links.length);
  console.log('TOTAL TODO', toDoLinks.length);
})();
