module.exports = () => {
  const links = [];
  document.querySelectorAll('h4>a').forEach(x => links.push({ href: x.href }));
  return links;
};
