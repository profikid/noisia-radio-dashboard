const puppeteer = require('puppeteer');
const fs = require('fs-extra');
const scrapeLinks = require('./scrape-links');

const feedUrl = 'http://feeds.noisia.nl/NoisiaRadio?format=xml';
const linksPath = '../data/links.json';

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto(feedUrl);
  await fs.ensureFile(linksPath);
  const curLinks = await fs.readJson(linksPath);
  const links = await page.evaluate(scrapeLinks);
  const linksToAdd = links.filter(x => !curLinks.map(y => y.href).includes(x.href));
  const newLinks = [...curLinks, ...linksToAdd];
  console.log('Links added:', linksToAdd.length);
  await fs.writeJson(linksPath, newLinks, { spaces: '\t' });
  await browser.close();
})();
