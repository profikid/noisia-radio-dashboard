import Vue from 'vue';
import ElementUI from 'element-ui';

import VueVega from 'vue-vega';
import VueGoodTablePlugin from 'vue-good-table';
import HighchartsVue from 'highcharts-vue';
import '../node_modules/vue-good-table/dist/vue-good-table.css';
import 'element-ui/lib/theme-chalk/index.css';

import App from './App.vue';
import router from './router';
import store from './store';

Vue.use(HighchartsVue);
Vue.use(VueVega);
Vue.use(VueGoodTablePlugin);
Vue.use(ElementUI);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
