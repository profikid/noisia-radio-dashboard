/* eslint-disable no-prototype-builtins */
/* eslint-disable no-param-reassign */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
export const findInLabel = (tracks, labelName) => tracks.filter(x => x.label.toLowerCase() === labelName.toLowerCase());
export const findInArtist = (tracks, artistName) => tracks.filter(x => x.artist.map(y => y.toLowerCase()).indexOf(artistName.toLowerCase()) >= 0);

Object.byString = (o, s) => {
  s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
  s = s.replace(/^\./, ''); // strip a leading dot
  const a = s.split('.');
  for (let i = 0, n = a.length; i < n; ++i) {
    const k = a[i];
    if (k in o) {
      o = o[k];
    }
  }
  return o;
};

export const agBy = (tracks, prop, modifier, sortOnKey = false) => {
  const sort = sortOnKey ? 'key' : 'count';
  const agg = tracks.reduce((p, c) => {
    let name = Object.byString(c, prop);
    if (modifier !== undefined) {
      name = modifier(name);
    }
    if (Array.isArray(c[prop])) {
      // eslint-disable-next-line prefer-destructuring
      name = c[prop][0];
    }
    if (!p.hasOwnProperty(name)) {
      p[name] = 0;
    }
    p[name]++;
    return p;
  }, {});
  return Object.keys(agg)
    .map(x => ({ key: x, count: agg[x] }))
    .sort((a, b) => b[sort] - a[sort]);
};
export default {
  agBy,
  findInArtist,
  findInLabel,
};
