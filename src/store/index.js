/* eslint-disable no-param-reassign */
/* eslint-disable no-plusplus */
import Vue from 'vue';
import Vuex from 'vuex';
import uuid from 'uuid';

import tracks from '../../data/tracks.json';
import { setFilter } from './action-types';
import { SET_FILTER } from './mutations-types';
import { agBy, findInArtist } from './helpers';

Vue.use(Vuex);
// Const
const CHART_TYPE = {
  PIE: 'pie',
  BAR: 'bar',
};
const DIMENSIONS = {
  ARTIST: 'artist',
  LABEL: 'label',
  TRACK: 'track',
  SEASON: 'episode.episode.season',
  EPISODE: 'episode.season.title',
  RELEASE_DATE: 'episode.releaseDate',
};

// Helpers
const chartData = (
  s,
  chartType = 'pie',
  dimension = 'artist',
  limit = s.limit.default,
  title = 'undefined title',
  modifier,
  sortOnKey,
) => {
  const chart = JSON.parse(JSON.stringify(s.charts[chartType]));
  chart.title.text = title;
  chart.series[0].data = agBy(s.tracks, dimension, modifier, sortOnKey)
    .map(x => ({ name: x.key, y: x.count }))
    .slice(0, limit);
  return chart;
};

const state = {
  tracks,
  filteredTracks: [],
  config: {
    baseUrl: '.netlify/functions/',
  },
  network: {
    nodes: [
      { id: 1, label: 'Node 1' },
      { id: 2, label: 'Node 2' },
      { id: 3, label: 'Node 3' },
      { id: 4, label: 'Node 4' },
      { id: 5, label: 'Node 5' },
    ],
    edges: [
      { id: 1, from: 1, to: 3 },
      { id: 2, from: 1, to: 2 },
      { id: 3, from: 2, to: 4 },
      { id: 4, from: 2, to: 5 },
      { id: 5, from: 3, to: 3 },
    ],
    options: {},
  },
  filter: {
    aggregrationBy: 'artist',
    counter: 0,
    labelSearch: 'division',
    artistSearch: 'Noisia',
  },
  aggregrationTypes: [
    { field: 'artist', label: 'Artist' },
    { field: 'label', label: 'Label' },
    { field: 'episode.title', label: 'Episode' },
  ],
  limit: {
    default: 25,
    artist: 50,
    track: 50,
    label: 50,
  },
  charts: {
    [CHART_TYPE.PIE]: {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie',
      },
      title: {
        text: 'TETTE!',
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: true,
            format: '<b>{point.name}</b>: {point.y} plays',
          },
        },
      },
      series: [
        {
          name: 'Brands',
          colorByPoint: true,
          // turboThreshold: 0,
          data: [],
        },
      ],
    },
    [CHART_TYPE.BAR]: {
      chart: {
        type: 'column',
      },
      title: {
        text: 'Monthly Average Rainfall',
      },
      subtitle: {
        text: 'Source: WorldClimate.com',
      },
      xAxis: {
        categories: [
          'Jan',
          'Feb',
          'Mar',
          'Apr',
          'May',
          'Jun',
          'Jul',
          'Aug',
          'Sep',
          'Oct',
          'Nov',
          'Dec',
        ],
        crosshair: true,
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Rainfall (mm)',
        },
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>'
            + '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true,
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0,
        },
      },
      series: [{
        name: 'Tokyo',
        data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4],

      }, {
        name: 'New York',
        data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3],

      }, {
        name: 'London',
        data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2],

      }, {
        name: 'Berlin',
        data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1],

      }],
    },
  },
};
const mutations = {
  [SET_FILTER]({ filter }, field) {
    filter.aggregrationBy = field;
    filter.counter++;
  },
};

const actions = {
  [setFilter](context, aggrType) {
    context.commit(SET_FILTER, aggrType.field);
  },
};

const getters = {
  agByTrack: s => chartData(s, CHART_TYPE.PIE, DIMENSIONS.TRACK, s.limit.track, `top ${s.limit.track} tracks`),
  agByArtist: s => chartData(s, CHART_TYPE.PIE, DIMENSIONS.ARTIST, s.limit.artist, `top ${s.limit.artist} artists`),
  agByLabel: s => chartData(s, CHART_TYPE.PIE, DIMENSIONS.LABEL, s.limit.label, `top ${s.limit.label} labels`),
  agBySeason: s => chartData(s, CHART_TYPE.PIE, DIMENSIONS.SEASON, 100, 'top episode dinges', undefined, true),
  agByEpisode: s => chartData(s, CHART_TYPE.PIE, DIMENSIONS.EPISODE, 100, 'top episode dinges', undefined, true),
  agByMonth: s => chartData(s, CHART_TYPE.PIE, DIMENSIONS.RELEASE_DATE, 100, 'plays per month', d => d.substr(0, d.length - 3)),
  barChartTest: s => s.charts[CHART_TYPE.BAR],
  dates: s => s.tracks.map(track => new Date(track.episode.releaseDate)),

  stats: s => ({
    totalTracks: s.tracks.length,
    episodes: 0,
  }),
  network: (s) => {
    const nodes = [];
    const edges = [];
    const options = {
      layout: {
        randomSeed: 191006,
        improvedLayout: false,
      },

      physics: {
        enabled: true,
        stabilization: {
          iterations: 500,
        },
      },
    };
    const map = new Map();
    const filteredTracks = findInArtist(tracks, s.filter.artistSearch);
    filteredTracks.forEach((track) => {
      let key = track.rawTrack;
      const artistId = uuid();
      if (!map.has(key)) {
        map.set(key, true);
        nodes.push({
          id: artistId,
          name: key,
          label: key,
          track,
        });
      }
      map.clear();

      key = track.episode.title;
      const labelId = uuid();
      if (!map.has(key)) {
        map.set(key, true);
        nodes.push({
          id: labelId,
          name: key,
          label: key,
          track,
        });
      }
      map.clear();
      key = track.episode.title;
      const episodeId = uuid();
      if (!map.has(key)) {
        map.set(key, true);
        nodes.push({
          id: episodeId,
          name: key,
          label: key,
          track,
        });
      }
      map.clear();

      edges.push({
        id: uuid(),
        from: artistId,
        to: episodeId,
      });
    });


    // tracks.forEach((track) => {
    //   const key = track.rawTrack;
    //   if (!map.has(key)) {
    //     map.set(key, true); // set any value to Map
    //     nodes.push({
    //       name: key,
    //       label: key,
    //     });
    //   }
    // });
    // map.clear();

    // tracks.forEach((track) => {
    //   const key = track.rawTrack;
    //   if (!map.has(key)) {
    //     map.set(key, true); // set any value to Map
    //     nodes.push({
    //       name: key,
    //       label: key,
    //     });
    //   }
    // });
    // map.clear();

    // console.log(nodes);
    // console.log(edges);

    return {
      nodes,
      edges,
      options,
    };
  },
};
const modules = {};


export default new Vuex.Store({
  modules, actions, getters, state, mutations,
});
