import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
  },
  {
    path: '/about',
    name: 'about',
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
  },
  {
    path: '/episodes',
    name: 'episodes',
    component: () => import(/* webpackChunkName: "episodes" */ '../views/Episodes.vue'),
  },
  {
    path: '/plays',
    name: 'plays',
    component: () => import(/* webpackChunkName: "plays" */ '../views/Plays.vue'),
  },
  {
    path: '/seasons',
    name: 'seasons',
    component: () => import(/* webpackChunkName: "seasons" */ '../views/Seasons.vue'),
  },
  {
    path: '/tracks',
    name: 'tracks',
    component: () => import(/* webpackChunkName: "tracks" */ '../views/Tracks.vue'),
  },
  {
    path: '/artists',
    name: 'artists',
    component: () => import(/* webpackChunkName: "artists" */ '../views/Artists.vue'),
  },
  {
    path: '/labels',
    name: 'labels',
    component: () => import(/* webpackChunkName: "labels" */ '../views/Labels.vue'),
  },
  {
    path: '/grid',
    name: 'grid',
    component: () => import(/* webpackChunkName: "grid" */ '../views/Grid.vue'),
  },
  {
    path: '/network-kitchensink',
    name: 'network-kitchensink',
    component: () => import(/* webpackChunkName: "network-kitchensink" */ '../views/NetworkKitchenSink.vue'),
  },
  {
    path: '/network',
    name: 'network',
    component: () => import(/* webpackChunkName: "network" */ '../views/Network.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
